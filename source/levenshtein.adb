package body Levenshtein is
	function Levenshtein_Distance(Source : String; Target : String) return Natural is
		type Cost_Buffer is array (0 .. Target'Length + 1) of Natural;
		Buffer_1, Buffer_2 : Cost_Buffer;
	begin
		if Source'Length = 0 then
			return Target'Length;
		elsif Target'Length = 0 then
			return Source'Length;
		end if;

		for i in Buffer_1'Range loop
			Buffer_1(i) := i;
		end loop;

		for i in Source'Range loop
			Buffer_2(0) := i;
			for j in Target'Range loop
				declare
					Cost : Natural;
				begin
					if Source(i) = Target(j) then
						Cost := 0;
					else
						Cost := 1;
					end if;

					Buffer_2(j) := Natural'Min(Buffer_2(j-1) + 1,
											   Natural'Min(Buffer_1(j) + 1,
														   Buffer_1(j-1) + Cost));
				end;
			end loop;

			declare
				Temp : Cost_Buffer;
			begin
				Temp := Buffer_1;
				Buffer_1 := Buffer_2;
				Buffer_2 := Buffer_1;
			end;
		end loop;
		return Buffer_2(Target'Length);
	end Levenshtein_Distance;
end Levenshtein;
